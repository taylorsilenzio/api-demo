// Useing babel for importing, exporting and other
//  features that are still not available in node.js.
//  This should always work on any version of node used.
require('babel-register');
require('./src');

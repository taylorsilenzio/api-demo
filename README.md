# API Project
An API with a single movie endpoint that requires authentication to access

## Modules
- `PouchDB` is a database that has been configured to run in-memory
- `Koa` is the web server framework
- `Babel` provide backwards compatibility with older Node.js versions
- `ESLint` guarantees the code is written using some of today's best practices

## Setup
- Install the dependencies
	- `npm install`
- Install PM2 globally
	-   `npm i -g pm2`

## Start Server
`pm2 start ecosystem.config.js && pm2 logs movie`

## Stop Server
`pm2 stop ecosystem.config.js`

## Testing API
- Import the provided environment and collection into Postman
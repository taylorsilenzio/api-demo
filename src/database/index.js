import PouchDB from 'pouchdb';
import PouchdbAdapterMemory from 'pouchdb-adapter-memory';

PouchDB.plugin(PouchdbAdapterMemory);

const db = new PouchDB('db', { adapter: 'memory' });

export default db;

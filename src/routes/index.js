import combineRouters from 'koa-combine-routers';
import movieRouter from './movie';

// Aggregate all routers into one
const router = combineRouters([
  movieRouter,
]);

export default router;

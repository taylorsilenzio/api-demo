import Router from 'koa-router';
import _ from 'lodash';
import moment from 'moment';
import db from '../database';

const router = new Router({ prefix: '/movie' });

router
  .get('/', async (ctx) => {
    // Fetch all available records
    const result = await db.allDocs({
      include_docs: true,
    });

    // Prepare records from db for viewing
    const records = _
      .chain(result)
      .get('rows')
      .map((i) => {
        const record = _.get(i, 'doc');
        record.releaseDate = moment(record.releaseDate).format('YYYY-MM-DD');
        return record;
      })
      .value();

    // Send prepared records
    ctx.body = { movies: records };
  })
  .get('/:id', async (ctx) => {
    const { id } = ctx.params;

    const record = await db.get(id, {
      latest: true,
    });

    // Send requested record
    ctx.body = { movie: record };
  })
  .post('/', async (ctx) => {
    const request = ctx.request.body;

    // Create one or multiple movie entries
    let result = await db.bulkDocs(request);

    // Remove "ok" field from results
    // eslint-disable-next-line no-param-reassign
    _.chain(result).each(i => delete i.ok).value();

    // Fetch new records
    result = await db.bulkGet({
      docs: Array.isArray(result) ? result : [result],
    });

    // Prepare records from db for viewing
    const records = _
      .chain(result)
      .get('results')
      .map((i) => {
        const record = _.get(i, 'docs[0].ok');
        record.releaseDate = moment(record.releaseDate).format('YYYY-MM-DD');
        return record;
      })
      .value();

    // Send prepared records
    ctx.body = { movie: records };
  })
  .delete('/:id', async (ctx) => {
    const { id } = ctx.params;

    // Fetch record since we ned the rev id for deleting
    const record = await db.get(id, {
      latest: true,
    });

    // Soft delete record
    const newRecord = await db.remove(record);

    // Send deleted record info
    ctx.body = newRecord;
  });

export default router;

import Koa from 'koa';
import logger from 'koa-logger';
import bodyParser from 'koa-bodyparser';
import error from 'koa-json-error';
import auth from 'koa-basic-auth';
import router from './routes';

const app = new Koa();

// Console logging
app.use(logger());

// Handle any errors
app.use(error());

// Automatically parse request bodies
app.use(bodyParser());

// Throw error for incompatible accept types headers
app.use(async (ctx, next) => {
  if (!ctx.accepts('application/json')) {
    ctx.throw(406, 'Not Acceptable');
  }

  await next();
});

// Handle all other invalid routes
app.use(async (ctx, next) => {
  try {
    await next();
    if (ctx.status === 404) {
        ctx.throw(404);
    }
  } catch (err) {
    ctx.status = err.status || 500;
    if (ctx.status === 404) {
      ctx.throw(404);
    } else {
      ctx.throw(404);
    }
  }
});

// Require proper authentication to proceed any further
app.use(async (ctx, next) => {
  try {
    await next();
  } catch (err) {
    if (err.status === 401) {
      ctx.status = 401;
      ctx.set('WWW-Authenticate', 'Basic realm=Authorization Required');
      ctx.body = { error: 'Incorrect username or password.' };
    } else {
      throw err;
    }
  }
});

// Only hardcoded temporarily
app.use(auth({ name: 'spark', pass: 'testing1' }));

// Apply the routes specificed in the router (./routes/index.js)
app.use(router());

console.log('Use http://localhost:3333 with the provided Postman collection and environment');

app.listen(3333);

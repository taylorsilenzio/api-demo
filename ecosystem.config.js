module.exports = {
  apps: [{
    name: 'movie',
    script: 'server.js',
    cwd: '.',
    watch: './src',
    ignore_watch: 'node_modules',
    watch_options: {
      followSymlinks: true,
    },
  }],
};
